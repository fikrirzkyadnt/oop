<?php
    require_once('animal.php');
    require_once('Ape.php');
    require_once('Frog.php');

    $sheep = new Animal("Shawn");

    echo "Animal Name : ",$sheep->name,"<br>";
    echo "Animal Legs : ",$sheep->legs,"<br>";
    echo "Animal Cold Blooded (True/False) : ",$sheep->cold_blooded,"<br><br>";

    $sungokong = new Ape("kera sakti");
    echo "Animal Name : ",$sungokong->name,"<br>";
    echo "Animal Legs : ",$sungokong->legs,"<br>";
    echo "Animal Cold Blooded (True/False) : ",$sungokong->cold_blooded,"<br>";
    $sungokong->yell();
    echo "<br><br>";

    $kodok = new Frog("buduk");
    echo "Animal Name : ",$kodok->name,"<br>";
    echo "Animal Legs : ",$kodok->legs,"<br>";
    echo "Animal Cold Blooded (True/False) : ",$kodok->cold_blooded, "<br>";
    $kodok->jump() ;
?>